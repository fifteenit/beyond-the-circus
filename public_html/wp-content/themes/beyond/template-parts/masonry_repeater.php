<?php /*?>					<?php if (get_sub_field('text_block_alignment') == 'Left' ) { ?>splitleft<?php }?>
<?php */?>
                
<?php
// check if the repeater field has rows of data
	if( have_rows('masonry') ) {
?>
	<div class="grid">
	<?php // loop through the row of data
		while (have_rows('masonry') ) { 
		the_row();
	?>
		<?php //disply a sub field value ?>
         <div class="grid-item 
		 <?php if ( get_sub_field('button') == 'two' ){?> grid-item--widthtwo<?php }?> 
		 <?php  if ( get_sub_field('button') == 'three' ) {?> grid-item--widththree<?php  }?>
         <?php if ( get_sub_field('button') == 'four' ) {?> grid-item--widthfour<?php  }?> 
		 <?php if ( get_sub_field('button') == 'five' ) {?> grid-item--heighttwo<?php  }?>
         <?php if ( get_sub_field('button') == 'six' ) {?> grid-item--heightthree<?php  }?>
          <?php if ( get_sub_field('button') == 'seven' ) {?> grid-item--heightfour<?php  }?>
         
         
         
         ">
      <a href="<?php the_sub_field('service_link'); ?>">

         
		 <div class="project-box">
       
     
     
				 <?php
                    $image = get_sub_field('image');
                    $size = 'large';
                        if ($image) {
                        echo wp_get_attachment_image( $image, $size );
                        }
                ?>
                
                <div class="project_overlay">
                <h2> <div class="boxfive"> <?php the_sub_field('name');?></div></h2>
                  <div class="boxfour">
	                  <p> Details </p><div class="arrow"><i class="fas fa-arrow-right"></i> </div>
                  </div>
             
        </div>
            </div>
            </a>
            
            
            
            
            </div>
            
<?php
		 };


};

?>	

    <script>
// external js: masonry.pkgd.js, imagesloaded.pkgd.js

var grid = document.querySelector('.grid');
var msnry;

imagesLoaded( grid, function() {
  // init Isotope after all images have loaded
  msnry = new Masonry( grid, {
    itemSelector: '.grid-item',
    columnWidth: '.grid-sizer',
    percentPosition: true
  });
});

	</script>

</div>