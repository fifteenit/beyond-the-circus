	<?php 
        $setColor =  get_sub_field('background_colour', $post->ID);
        $color = $setColor;
        $rgb = hex2rgba($color);
        $rgba = hex2rgba($color, 1);
        $border = get_sub_field('border_location');
    ?>
        <?php if ( $rgba ) { ?>
            <?php if ( get_sub_field('text_colour' ) ) { ?>
                <div class="section  colourbg <?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="background:<?php echo $rgba ?>;color:<?php the_sub_field('text_colour');?>;<?php if (get_sub_field('border')){?>
                    <?php if (in_array('top', get_sub_field('border_location'))) {?>
                        border-top-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>
                        border-bottom-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('left', get_sub_field('border_location'))) {?>
                        border-left-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('right', get_sub_field('border_location'))) {?>
                        border-right-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                <?php } ?>"
				<?php if (get_sub_field( 'anchor' ) ) { ?>
                    data-anchor="<?php the_sub_field( 'anchor' ); ?>"
                <?php } ?>
				>
            <?php } else { ?>
                <div class="section  colourbg <?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="background:<?php echo $rgba ?>;<?php if (get_sub_field('border')){?>
                    <?php if (in_array('top', get_sub_field('border_location'))) {?>
                        border-top-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>
                        border-bottom-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('left', get_sub_field('border_location'))) {?>
                        border-left-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('right', get_sub_field('border_location'))) {?>
                        border-right-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                <?php } ?>"
				<?php if (get_sub_field( 'anchor' ) ) { ?>
                    data-anchor="<?php the_sub_field( 'anchor' ); ?>"
                <?php } ?>
				>
            <?php } ?>
        <?php } else { ?>
            <div class="section  <?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="
            <?php if (get_sub_field('border')){?>
                    <?php if (in_array('top', get_sub_field('border_location'))) {?>
                        border-top-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>
                        border-bottom-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('left', get_sub_field('border_location'))) {?>
                        border-left-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('right', get_sub_field('border_location'))) {?>
                        border-right-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                <?php } ?>"
				<?php if (get_sub_field( 'anchor' ) ) { ?>
                    data-anchor="<?php the_sub_field( 'anchor' ); ?>"
                <?php } ?>
				>
        <?php } ?>
		<?php if (get_sub_field('slanted_edges') == "Top" || get_sub_field('slanted_edges') == "Both" ) {?>
            <div class="slanted-top" style=" <?php if (get_sub_field('slanted_top_section_angle')) { ?>transform:skewY(<?php the_sub_field('slanted_top_section_angle');?>deg);<?php } ?><?php if(get_sub_field('slanted_top_section_colour')) { ?>background:<?php the_sub_field('slanted_top_section_colour');?><?php } ?>"></div>
        <?php } ?>
        	<div class="pp-tableCell" style="height:100%">
                <div class="content">
                    <div class="">
                        <?php if (get_sub_field('section_title')) { ?>
                            <h2 class="section-title2"><?php the_sub_field('section_title');?></h2>
                        <?php } ?>
                         <?php if (get_sub_field('facebook') || get_sub_field('twitter') || get_sub_field('instagram') || get_sub_field('snapchat') || get_sub_field('google_plus')) { ?>
                        	<div class="social socialblock">
							<?php if (get_sub_field('facebook')) { ?>
                                <a class="facebook" target="_blank" href="<?php get_sub_field('facebook') ?>"><i class="fab fa-facebook-square" aria-hidden="true"></i></a>
                            <?php } ?>
							<?php if (get_sub_field('twitter')) { ?>
                                <a class="twitter" target="_blank" href="<?php get_sub_field('twitter') ?>"><i class="fab fa-twitter-square" aria-hidden="true"></i></a>
                            <?php } ?>
							<?php if (get_sub_field('google_plus')) { ?>
                                <a class="googleplus" target="_blank" href="<?php get_sub_field('google_plus') ?>"><i class="fab fa-google-plus-square" aria-hidden="true"></i></a>
                            <?php } ?>
							<?php if (get_sub_field('snapchat')) { ?>
                                <a class="snapchat" target="_blank" href="<?php get_sub_field('snapchat') ?>"><i class="fab fa-snapchat-ghost" aria-hidden="true"></i></a>
                            <?php } ?>
							<?php if (get_sub_field('instagram')) { ?>
                                <a class="instagram" target="_blank" href="<?php get_sub_field('instagram') ?>"><i class="fab fa-instagram" aria-hidden="true"></i></a>
                            <?php } ?>
							<?php if (get_sub_field('linkedin')) { ?>
                                <a class="linkedin" target="_blank" href="<?php get_sub_field('linkedin') ?>"><i class="fab fa-linkedin" aria-hidden="true"></i></a>
                            <?php } ?>
							<?php if (get_sub_field('website')) { ?>
                                <a class="website" target="_blank" href="<?php get_sub_field('website') ?>"><i class="fas fa-globe" aria-hidden="true"></i></a>
                            <?php } ?>


                            </div>
                      	<?php } ?>
                    </div>
              	</div>
            </div>
        </div>
