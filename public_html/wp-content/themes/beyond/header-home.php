<?php
/**
 * Header Template
 * @file           header.php
 * @package        Beyond the Circus
 * @filesource     wp-content/themes/beyond/header.php
 * @since          Beyond the Circus 1.0
*/
?>
<!doctype html>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:700|Roboto:400,700" rel="stylesheet"> 
</head>
<body>
	<div class="sitewrapper">
    	 
   
    	<div class="header wrapper">
        	<div class="content">
            	<div class="flexwrapper alignmiddle spacebetween">
                	<div class="site-logo">
						<?php $custom_logo_id = get_theme_mod( 'custom_logo' );
                                $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                        ?>
                        <?php if ( $custom_logo_id ) { ?>
                            <div class="logo">
                                <a href="<?php echo get_home_url(); ?>" rel="home"><img src="<?php echo $image[0]; ?>" alt="<?php echo get_bloginfo('name');?> - <?php echo get_bloginfo('description'); ?>"/></a>
                            </div>
                        <?php } else { ?>
                            <h2 class="site-title"><a href="<?php echo get_home_url(); ?>" rel="home"><?php echo get_bloginfo('name');?></a></h2><br/>
                            <span class="site-description"><?php echo get_bloginfo('description'); ?></span>
                        <?php } ?>
    				</div>
                    <nav class="mainnavmenu">
                        <?php wp_nav_menu( array('theme_location' => 'main'));?>
                    </nav>
                </div>
            </div>
        </div>
    